package com.nagarro.demo.Assignment1;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.StringTokenizer;

public class DataController {
	ArrayList<DataModel> tshirtList = new ArrayList<>();
	ArrayList<String> arr;
	DataView view = new DataView();
	String[] filePath = {
			"C:\\Assigment Links\\Adidas.csv",  "C:\\Assigment Links\\Nike.csv", "C:\\Assigment Links\\Puma.csv"
	};
	
	public void searchData(String color, String size, String gender) {
		for(String fPath : filePath) {
			Path file = Path.of(fPath);
			Scanner scn = null;
			try {
				scn = new Scanner(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				while(scn.hasNext()) {
					String row = scn.nextLine().toUpperCase().toString();
					if(!row.isEmpty()) {
						StringTokenizer token = new StringTokenizer(row, "|");
						arr = new ArrayList<>(row.length());
						while(token.hasMoreTokens()) {
							arr.add(token.nextToken());
						}
						if(arr.get(2).equalsIgnoreCase(color) && arr.get(4).equalsIgnoreCase(size) && arr.get(3).equalsIgnoreCase(gender)) {
							DataModel data = new DataModel(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4), arr.get(5), Double.parseDouble(arr.get(6)), arr.get(7).charAt(0)); 
							tshirtList.add(data);
						}				
					}
				}
			
		}		
	}
	
	public void manageData(String preference) {
		if(preference.equalsIgnoreCase("price")) {
			Collections.sort(tshirtList, new Comparator<DataModel>() {
				@Override
				public int compare(DataModel o1, DataModel o2) {
					// TODO Auto-generated method stub
					return (int) (Float.parseFloat (o1.getPrice()) - Float.parseFloat (o2.getPrice()));
				}				
			});
		} else if(preference.equalsIgnoreCase("rating")) {
			Collections.sort(tshirtList, new Comparator<DataModel>() {
				@Override
				public int compare(DataModel o1, DataModel o2) {
					// TODO Auto-generated method stub
					return (int) (o1.getRating() - o2.getRating());
				}				
			});
		} else if(preference.equalsIgnoreCase("both")) {
			Collections.sort(tshirtList, new Comparator<DataModel>() {
				@Override
				public int compare(DataModel o1, DataModel o2) {
					// TODO Auto-generated method stub
					if(o1.getPrice() == o2.getPrice()) 
						return (int) (o1.getRating() - o2.getRating());
					else
						return (int) (Float.parseFloat (o1.getPrice()) - Float.parseFloat (o2.getPrice()));
				}				
			});
		} else {
			System.out.println("Wrong Preference");
		}
		view.displayTShirt(tshirtList);
	}
}
