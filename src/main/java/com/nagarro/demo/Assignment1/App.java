package com.nagarro.demo.Assignment1;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class App 
{
	public static Scanner scn = new Scanner(System.in);
    public static void main( String[] args ) throws FileNotFoundException
    {
    	String colorOfTShirt, gender, preferenceChoice, size;
    	System.out.println("Enter T-Shirt Details");
    	System.out.print("Enter Color : ");
    	colorOfTShirt = scn.nextLine();
    	System.out.print("Enter Size : ");
    	size = scn.nextLine();
    	System.out.print("Enter Gender : ");
    	gender = scn.nextLine();
    	System.out.print("Enter Output Preference (Price,Rating,Both) : ");
    	preferenceChoice = scn.nextLine();
    	
    	DataController dc = new DataController();
    	
    	dc.searchData(colorOfTShirt, size, gender);
    	
    	dc.manageData(preferenceChoice);
    }
}
